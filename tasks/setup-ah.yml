---
- name: Create .ssh directory for root
  ansible.builtin.file:
    path: /root/.ssh
    state: directory
    mode: "0700"
    owner: root
    group: root

- name: Deploy ssh_config for root
  ansible.builtin.lineinfile:
    path: /root/.ssh/config
    mode: "0600"
    owner: root
    group: root
    create: true
    regex: "^StrictHostKeyChecking"
    line: "StrictHostKeyChecking no"
    insertbefore: BOF

- name: Create ssh key pair for root
  community.crypto.openssh_keypair:
    path: /root/.ssh/id_rsa
    mode: "0600"
    owner: root
    group: root

- name: Slurp public key
  ansible.builtin.slurp:
    path: /root/.ssh/id_rsa.pub
  register: public_key

- name: Create ansible user on Automation Hub
  ansible.builtin.user:
    name: ansible
    create_home: true
  delegate_to: "{{ controller_ah_instance_name }}"

- name: Add public key to authorized keys on automation hub
  ansible.posix.authorized_key:
    user: ansible
    key: "{{ public_key.content | b64decode }}"
    state: present
  delegate_to: "{{ controller_ah_instance_name }}"

- name: Enable sudo without password for user ansible
  ansible.builtin.lineinfile:
    path: /etc/sudoers.d/ansible
    line: "ansible   ALL=(ALL)       NOPASSWD: ALL"
    mode: "0600"
    owner: root
    group: root
    create: true
    state: present
  when: controller_ah_enable | bool
  delegate_to: "{{ controller_ah_instance_name }}"

- name: Add private Automation Hub to inventory
  ansible.builtin.lineinfile:
    dest: /opt/ansible-automation-platform/installer/inventory
    owner: root
    create: false
    group: root
    state: present
    mode: "0644"
    insertafter: '^\[automationhub\]'
    line: "{{ controller_ah_fqdn }} ansible_user=ansible"

- name: Set automation hub admin_password
  ansible.builtin.lineinfile:
    dest: /opt/ansible-automation-platform/installer/inventory
    owner: root
    create: false
    group: root
    state: present
    mode: "0644"
    regexp: ^automationhub_admin_password
    line: automationhub_admin_password='{{ controller_admin_password }}'

- name: Set automation hub pg_password
  ansible.builtin.lineinfile:
    dest: /opt/ansible-automation-platform/installer/inventory
    owner: root
    create: false
    group: root
    state: present
    mode: "0644"
    regexp: ^automationhub_pg_password
    line: automationhub_pg_password='{{ controller_pg_password }}'

- name: Set automation hub automationhub_pg_host
  ansible.builtin.lineinfile:
    dest: /opt/ansible-automation-platform/installer/inventory
    owner: root
    create: false
    group: root
    state: present
    mode: "0644"
    regexp: ^automationhub_pg_host
    line: automationhub_pg_host='{{ controller_fqdn }}'
